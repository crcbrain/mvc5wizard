﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MVCWizard.Models
{
    public class SaleItem
    {
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Step1")]
        public string Step1 { get; set; }
        [DisplayName("Step2")]
        public string Step2 { get; set; }
        [DisplayName("Step3")]
        public string Step3 { get; set; }
        [DisplayName("Selected")]
        public bool Selected { get; set; }
    }
}