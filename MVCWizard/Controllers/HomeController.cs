﻿using MVCWizard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCWizard.Controllers
{
    public class HomeController : Controller
    {

        public static SaleItem SaleItem;
        
        
        static HomeController()
        {
            SaleItem = new SaleItem { Name = "test1", Selected = false };

        }

        public ActionResult Index()
        {
            return View(SaleItem);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Step3(SaleItem item)
        {
            SaleItem = item;

            return View("_Step3", item);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Step2(SaleItem item)
        {
            SaleItem = item;

            return View("_Step2", item);
        }
    }
}